package com.signup;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


public class Main extends Application {

    /**
     * {@inheritDoc}
     * @param primaryStage      Accepts Stage.
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        primaryStage.setTitle("Store Management System");
        primaryStage.getIcons().add(new Image("/view/resources/img/brand/fav.png"));
        primaryStage.setScene(new Scene(root, 1280, 800));
        primaryStage.show();

    }

  
    public static void main(String[] args) {
        launch(args);
    }
}
